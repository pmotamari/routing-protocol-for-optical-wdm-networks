#include<stdio.h>// <ESC>linemuner G to go tthat corrosponding line number 
#include<stdlib.h>
#define LINE printf("\n");
#define MAX 1000.0
#define CMAX 35000
#define COST_ZERO 100
#define NO_OF_VERTICES 6
#define NO_OF_EDGES 10
#define NO_OF_WAVELENGTHS 6

//int i=6;//total no.of vertices in the topology
int edgenumber[ NO_OF_VERTICES ][NO_OF_VERTICES ];//6 is the number of vertices
int dest;//to be taken from the user;
int source;
int wavelengthassigned;
struct vertex1
{
int index;
float value;
};
struct vertex1 dijkstra_vertex[NO_OF_VERTICES ];//6 is no.of vertices
struct allpath
{
int pathno;
struct allpath *next;
int path[];
};// for holding the paths from source to destination

struct alpathvertex
{
int index;
struct alpathvertex *next;
};//parameters used in all_paths function 
int countpath=0;
typedef struct alpathvertex apvertex;

int edge[NO_OF_VERTICES ][NO_OF_VERTICES ] ={
			{0,1,1,0,1,0},
			{1,0,1,0,1,0},
			{1,1,0,1,1,0},
			{0,0,1,0,1,1},
			{1,1,1,1,0,1},
			{0,0,0,1,1,0}
		};


int edgecost[NO_OF_VERTICES ][NO_OF_VERTICES ]= {
			{0,6,6,CMAX,6,CMAX},
			{6,0,6,CMAX,6,CMAX},
			{6,6,0,6,6,CMAX},
			{CMAX,CMAX,6,0,6,6},
			{6,6,6,6,0,6},
			{CMAX,CMAX,CMAX,6,6,0}
		};

struct xy
{
float  min_cost;
struct xy *next;
int path1[NO_OF_VERTICES ];// we can't declare variable length arrays at file scope
int path2[NO_OF_VERTICES ];
};


typedef struct xy twopath;

twopath *head2 =NULL , *rear2 = NULL;

void all_paths(apvertex *,int []);//function to generate the paths usong add path function
int check(int ,int []);// check whether th int(edge) is there in the array
void addpath(int[],int,struct allpath **,struct allpath **);//adds a path to the stack
void  printpath(struct allpath *,float []/*edgecost*/);
void initialize(int ,struct vertex1 []);
void heapform(struct vertex1 []);
float  dijkstra(int [][NO_OF_VERTICES],int [][NO_OF_VERTICES],struct vertex1 [],int [],float []/*edgeweights*/);
int w_assign(int [],int [][NO_OF_VERTICES],int [],float []);


struct allpath *head1=NULL,*rear = NULL;//head1 points to the head of the stack and rear points to the bootom most element of the stack

int main()
{
int no_of_edges;
int j,k,num=0;//initializing the edgenumber array with -1 
for(j=0;j< NO_OF_VERTICES ;j++)
{
for(k=0;k< NO_OF_VERTICES ;k++)
{
edgenumber[j][k]=-1;
}
}

j=0;k=0;
for(j=0;j< NO_OF_VERTICES ;j++)
{
for(k=j+1;k< NO_OF_VERTICES ;k++)
{
	if(edge[j][k]==1)
	{
	edgenumber[j][k]=num;
	edgenumber[k][j]=num;
	num = num+1;
	}	

}
}
no_of_edges = num;
float  newedgecost[NO_OF_EDGES];
int num1=0;// for assigning edge capacities to the newedgecapacity array
for(j=0;j< NO_OF_VERTICES ;j++)
{
for(k=j+1;k< NO_OF_VERTICES ;k++)
{
	if(edge[j][k]==1)
	{
	newedgecost[num1]=edgecost[j][k];
	printf("Edge number is %d and cost is %f\n",num1,newedgecost[num1]);
	num1 = num1+1;
	}	

}
}


int e_wave[ NO_OF_EDGES ][ NO_OF_WAVELENGTHS ]={
				
			{1,1,1,1,1,1},
			{1,1,1,1,1,1},
			{1,1,1,1,1,1},
			{1,1,1,1,1,1},
			{1,1,1,1,1,1},
			{1,1,1,1,1,1},
			{1,1,1,1,1,1},
			{1,1,1,1,1,1},
			{1,1,1,1,1,1},
			{1,1,1,1,1,1}

			};

int ecapacity[ NO_OF_EDGES ]={3,3,4,3,5,2,3,3,4,3};
int arr[NO_OF_VERTICES];
//initialize head2,rear2 and head1,rear1 to null
int rand_i,rand_j;
int dropped_packets=0;
struct a_packet
{
int id;
int edges1[NO_OF_VERTICES];
int edges2[NO_OF_VERTICES];
int w1_assigned;
int w2_assigned;
struct a_packet *next;
};

typedef struct a_packet assigned_packet;

assigned_packet *a_head=NULL;
assigned_packet *a_tail=NULL;
assigned_packet *a_temp;

int c3,c4,c5,c6,c7,id;
for(rand_j=0;rand_j<10;rand_j++)
{
printf("\n\n*************MAIN ITERATION %d************\n",rand_j);

c3=0;c4=0;
while(c3<5)//No.of packets to be relesed wrtie as c3<x
{
if(a_head!=NULL)
{
if(a_head->id==1)
{
//printf("\n ahead is =%u\n",a_head);
c5 = a_head->w1_assigned;
printf("Wave length assigned to primary path is %d\n",c5);
c6 = a_head->w2_assigned;
printf("Wave length assigned to secondary path is %d\n",c6);
//printf("\n");
	c4=0;
	while(a_head->edges1[c4]!=-1)
	{
		c7 = a_head->edges1[c4];
		e_wave[c7][c5]=e_wave[c7][c5]+1;
	printf("%d,",c7);
	c4++;
		newedgecost[c7]++;

	}
printf("\n");
	c4=0;
	while(a_head->edges2[c4]!=-1)
	{
		c7 = a_head->edges2[c4];
		e_wave[c7][c6]= e_wave[c7][c6]+1;
		c4++;
		newedgecost[c7]++;
		printf("%d,",c7);
	}
	
	if(a_head->next==NULL)
	{	
	a_tail=NULL;
	}

printf("\n\nPACKET WAS DELETED WITH ID=1\n");

a_temp = a_head;
a_head = a_head->next;
//free(a_temp);
c3++;
}//end of if corresponds to id==1
else
{
//printf("\n ahead is =%u\n",a_head);
c5 = a_head->w1_assigned;
printf("wave length assigned to the primary path is %d\n",c5);
//printf("\n");
	c4=0;
	while(a_head->edges1[c4]!=-1)
	{
		c7 = a_head->edges1[c4];
		e_wave[c7][c5]=e_wave[c7][c5]+1;
	printf("%d,",c7);
	c4++;
		newedgecost[c7]++;

	}
printf("\n");

printf("\nPACKET WAS DELETED WITH ID=0\n");

if(a_head->next==NULL)
	{	
	a_tail=NULL;
//break;
	}

a_temp = a_head;
a_head = a_head->next;
//free(a_temp);
c3++;
}//else corresponds to id==0
}//end of if
else
{
break;
}

}//end of while

printf("*****printing e_wave******\n");
for(c3=0;c3<10;c3++)
{
 for(c4=0;c4<6;c4++)
  {
printf("%d",e_wave[c3][c4]);
  }
printf("\n");
}


c3=0;c4=0;

for(rand_i=0;rand_i<10;rand_i++)
{
source = (rand()%5);
dest = (rand()%5);
id = (rand()%2);
while(1)
{
if((dest== source)||(dest== source+1))
{
dest = (rand()%5);
}
else
{
break;
}
}


head2=NULL;rear2=NULL;head1 = NULL;rear= NULL;

initialize(source,dijkstra_vertex);

apvertex aver[NO_OF_VERTICES];

for(j=0;j<NO_OF_VERTICES;j++)
{
aver[j].index= j;// initialize the array
arr[j]=-1;
}
printf("***********ITERATION %d **********\n",rand_i);
printf("SOURCE IS %d, and DESTINATION is %d, ID is %d \n",source,dest,id);
countpath=0;//to get only four paths
all_paths(&(aver[source]),arr);// sending the source
rear->next = NULL;//to mark the end of stack
printf("\n%u",rear->next);
printpath(head1,newedgecost);
rear2->next = NULL;
//printf("\n%u,%u\n",head2,rear2->next);
j=0;
twopath *ahead2;
ahead2 = head2;
int j34=0;
while(ahead2!=NULL)
{
printf("\n*****THIS WHILE LOOP IS CALLED*****\n");
if(ahead2->min_cost>=100)
{
ahead2=ahead2->next;
j34++;
printf("entered here\n");
}

else if(id==1)
{
int path_assigned;
int assign1  = w_assign(ahead2->path1,e_wave,ecapacity,newedgecost);// e_wave ,ecapacity,newedgecost will be modified after successuful assignment of call
int assign2 = w_assign(ahead2->path2,e_wave,ecapacity,newedgecost);
int c,c1,c2,c8;

//here we need to release the wavelengths that were consumed either in assigning primary path (assgn1) or secondary path (assign2)


	if((assign1!=0)&&(assign2==0))//if primary path has got some assignment and its alternate didn't get any assignment
	{
	c=0;
	while(ahead2->path1[c+1]!=-1)
	{
	c1 = ahead2->path1[c];c2 = ahead2->path1[c+1];
	c8 = edgenumber[c1][c2];
	newedgecost[c8]= newedgecost[c8]+1;
	e_wave[c8][assign1-1]++;
	c++;
	}//end of while
ahead2= ahead2->next;
//j34++;
	}//end of if


	else if((assign1==0) &&(assign2!=0))//if secondary path was assigned and primary path was not assigned
	{
	c=0;
	while(ahead2->path2[c+1]!=-1)
	{
	c1 = ahead2->path2[c];c2 = ahead2->path2[c+1];
	c8 = edgenumber[c1][c2];
	newedgecost[c8]= newedgecost[c8]+1;
	e_wave[c8][assign2-1]++;
	c++;
	
	}//end of while
	ahead2= ahead2->next;
//j34++;
	}// end of if


else if((assign1==0)&&(assign2==0))
{
ahead2 = ahead2->next;
}

else if(a_tail==NULL)
	{
	a_tail = (assigned_packet *)malloc(sizeof(assigned_packet));
	a_tail->w1_assigned = assign1-1;
	a_tail->w2_assigned = assign2-1;
	a_tail->id=1;
	printf("primary path is\n");
	c=0;
	while(ahead2->path1[c]!=-1)
	{
	printf("%d,",ahead2->path1[c]);
	c++;
	}
	printf("\n");
	c=0;
	while(ahead2->path1[c+1]!=-1)
	{
	//printf("%d,%d,",ahead2->path1[c],ahead2->path1[c+1]);
	c1 = ahead2->path1[c]; c2 = ahead2->path1[c+1];
	a_tail->edges1[c]=edgenumber[c1][c2]; 
	c++;
	}
	a_tail->edges1[c]=-1;
printf("\n");
	c=0;
	printf("secondary path is\n");
	while(ahead2->path2[c]!=-1)
	{
	printf("%d,",ahead2->path2[c]);
	c++;
	}
	printf("\n");
	//printf("\n*****FROM ASSIGNINH****\n");
	c=0;
	while(ahead2->path2[c+1]!=-1)
	{
	//printf("%d,%d,",ahead2->path2[c],ahead2->path2[c+1]);
	c1= ahead2->path2[c]; c2 = ahead2->path2[c+1];
	a_tail->edges2[c]= edgenumber[c1][c2];
	//printf("%d,",a_tail->edges2[c]);
	c++;
	}
	printf("\n");
	a_tail->edges2[c]=-1;
		
	a_head = a_tail;
		
	printf("\n PACKETS WERE APPENDED and primary wavelength is %d,secondary wavelength is %d\n\n",assign1-1,assign2-1);
	break;
	}

	else if(a_tail!=NULL)
	{
	a_tail->next = (assigned_packet *)malloc(sizeof(assigned_packet));
	a_tail->next->w1_assigned = assign1-1;
	a_tail->next->w2_assigned = assign2 -1;
	a_tail->next->id =1;
	//printf("\n******from ****\n");
	c=0;
	printf("primary path is\n");
	while(ahead2->path1[c]!=-1)
	{
	printf("%d,",ahead2->path1[c]);
	c++;
	}
	printf("\n");
	c=0;
	while(ahead2->path1[c+1]!=-1)
	{
	//printf("%d,%d,",ahead2->path1[c],ahead2->path1[c+1]);
	c1 = ahead2->path1[c]; c2 = ahead2->path1[c+1];
	a_tail->next->edges1[c]=edgenumber[c1][c2]; 
	//printf("%d,",a_tail->next->edges1[c]);
	c++;
	}
	//printf("\n");
	a_tail->next->edges1[c]=-1;
	//printf("\n");
	
	c=0;
	printf("secondary path is\n");
	while(ahead2->path2[c]!=-1)
	{
	printf("%d,",ahead2->path2[c]);
	c++;
	}
	printf("\n");
	c=0;
	while(ahead2->path2[c+1]!=-1)
	{
		//printf("%d,%d,",ahead2->path2[c],ahead2->path2[c+1]);
	c1= ahead2->path2[c]; c2 = ahead2->path2[c+1];
	a_tail->next->edges2[c]= edgenumber[c1][c2];
	c++;
	}
	a_tail->next->edges2[c]=-1;
		//printf("\n");
	a_tail = a_tail->next;
	printf("\n PACKETS WERE APPENDED and primary wavelength is %d,secondary wavelength is %d\n\n",assign1-1,assign2-1);
	break;
	}
}//end of first else in the while loop corrsponds to id==1

else
{
int c,c1,c2;
int assign1 =  w_assign(ahead2->path1,e_wave,ecapacity,newedgecost);
if(assign1==0)
{
ahead2=ahead2->next;
}

else if(a_tail==NULL)
	{
	a_tail = (assigned_packet *)malloc(sizeof(assigned_packet));
	a_tail->w1_assigned = assign1-1;
	a_tail->id =0;
	printf("only primary path is assigned as id=0\n");
	c=0;
	while(ahead2->path1[c]!=-1)
	{
	printf("%d,",ahead2->path1[c]);
	c++;
	}
	printf("\n");
	c=0;
	while(ahead2->path1[c+1]!=-1)
	{
	//printf("%d,%d,",ahead2->path1[c],ahead2->path1[c+1]);
	c1 = ahead2->path1[c]; c2 = ahead2->path1[c+1];
	a_tail->edges1[c]=edgenumber[c1][c2]; 
	c++;
	}
	a_tail->edges1[c]=-1;
printf("\n");
	c=0;
		
	a_head = a_tail;
	
	printf("\n PACKETS WAS APPENDED and primary wavelength is %d\n\n",assign1-1);
	break;
	
}

else//a_tail!=NULL
{
a_tail->next = (assigned_packet *)malloc(sizeof(assigned_packet));
	a_tail->next->w1_assigned = assign1-1;
	a_tail->next->id =0;
	//printf("\n******from ****\n");
	c=0;
	printf("only primary path is assigned as id=0\n");
	while(ahead2->path1[c]!=-1)
	{
	printf("%d,",ahead2->path1[c]);
	c++;
	}
	printf("\n");
	c=0;
	while(ahead2->path1[c+1]!=-1)
	{
	//printf("%d,%d,",ahead2->path1[c],ahead2->path1[c+1]);
	c1 = ahead2->path1[c]; c2 = ahead2->path1[c+1];
	a_tail->next->edges1[c]=edgenumber[c1][c2]; 
	//printf("%d,",a_tail->next->edges1[c]);
	c++;
	}
	//printf("\n");
	a_tail->next->edges1[c]=-1;
	//printf("\n");
printf("\n PACKET WAS APPENDED and primary wavelength is %d\n\n",assign1-1);
	a_tail= a_tail->next;
	break;
}
}//end of second else in the while loop correspponds to id==0

}//while loop
if(ahead2==NULL)
{
dropped_packets++;
}
}//end of inner for loop 
}//end of outer for loop

printf("NO.OF DROPPED PACKETS IS %d\n",dropped_packets);
}//end of main

void all_paths(apvertex *vertex,int gb[])
{
//static int countpath = 0;
int gb1[NO_OF_VERTICES];//repository 
//printf("enetered all_paths with vertex %d\n",vertex->index);
int size=0,h;
h = vertex->index;
apvertex *head,*x;
x = (apvertex *)malloc(sizeof(apvertex));
head = x;
int j=0;
while(gb[j]!=-1)
{
//printf("%d",gb[j]);
j=j+1;
}
gb[j]=vertex->index;// update global array
//printf("%d",gb[j]);
//LINE
for(j=0;j<NO_OF_VERTICES;j++)
{

 if(edge[h][j]==1)
 {
  if(j==dest)
   {
   countpath = countpath+1;
   if(countpath>4)//k-paths
   {
   return;
   }
   addpath(gb,j,&rear,&head1);
//printf("from all_pathss(),addpath was called");
}
  else
   {
    if(!check(j,gb))
      {
        x->next = (apvertex *)malloc(sizeof(apvertex));
	x->next->index = j;
	x= x->next;

      }
 }

 }
}
//}
//}
//}
//}
//}

x->next = NULL;

while(head->next!=NULL)
{
{//block
int j;
for(j=0;j<NO_OF_VERTICES;j++)
{
gb1[j]=gb[j];
}
}//block
all_paths(head->next,gb1);
head= head->next;
}
}

int check(int j,int ar[])
{
//printf("enetered check\n");
int k=0;
while(ar[k]!=-1)
 { 
	if(ar[k]==j)
	{
	return 1;
	}
 k = k+1;
 }
return 0;
}

void addpath(int ar[],int j,struct allpath **x,struct allpath **hea)
{
if((*x)==NULL)
{
(*x)= (struct allpath *)malloc(sizeof(struct allpath));
(*hea)=(*x);

int k=0;
while(ar[k]!=-1)
{
((*x)->path[k])=ar[k];
k = k+1;
}

(*x)->path[k]=j;
(*x)->path[k+1]=-1;

}

else
{

(*x)->next = (struct allpath *)malloc(sizeof(struct allpath));

int k=0;
while(ar[k]!=-1)
{
(*x)->next->path[k]=ar[k];
k = k+1;
}

(*x)->next->path[k]=j;
(*x)->next->path[k+1]=-1;
(*x)= (*x)->next;

}

}

void  printpath(struct allpath *x,float e_cost1[])
{
//printf("entered printpath\n");
if(x==NULL)
{return ;
}

if(x!=NULL)
{
twopath *temp;
//printf("x=%u,x->next = %u,",x,x->next);
int k,k1,e_present[NO_OF_VERTICES][NO_OF_VERTICES];
struct vertex1 vertex[NO_OF_VERTICES];
for(k=0;k<NO_OF_VERTICES;k++)
{
vertex[k]=dijkstra_vertex[k];// getting contents of global array dikstra_vertex into vertex
}
for(k=0;k<NO_OF_VERTICES;k++)
{
for(k1=0;k1<NO_OF_VERTICES;k1++)
{
e_present[k][k1]=edge[k][k1];
}
}
twopath *initial_t2;
initial_t2 = rear2;
if(rear2==NULL)
{
rear2= (twopath *)malloc(sizeof(twopath));
head2 = rear2;
temp = rear2;
}

else
{
rear2->next  = (twopath *)malloc(sizeof(twopath));
temp = rear2->next;
//temp = rear2->next;
}
int j=0,a,b,k3;
float totalcost=0,pathcost,alternatepathcost;
int is_cost_zero=0;//if true else 0
printf("The primary  path is ");
while(x->path[j]!=-1)
{
temp->path1[j]=x->path[j];
printf("%d-",x->path[j]);
if(x->path[j+1]!=-1)
{
a = x->path[j];//get the vertex number
b = x->path[j+1];
k3 = edgenumber[a][b];
if(e_cost1[k3]!=0)
{
totalcost = totalcost +(1/ e_cost1[k3]); 
printf("(%f,%d),",e_cost1[k3],k3);
}
else
{
printf("(%d,%f,%d)",b,e_cost1[k3],k3);
is_cost_zero =1;
printf("\nprimary path is not available as cost become zero\n\n");
temp->min_cost = COST_ZERO;
break;
}
e_present[a][b] = 0;
e_present[b][a]= 0;

}
j = j+1;
}//end of while
if(is_cost_zero!=1)
{
temp->path1[j]=-1;
pathcost = totalcost;
printf(" and the cost is %f",pathcost);
printf("\n");
int path[NO_OF_VERTICES];
printf("Alternate path is ");
//printf("Dijkstra was called\n");
alternatepathcost = dijkstra(edgenumber,e_present,vertex,path,e_cost1);
//filling second or alternate path
j=0;
while(path[j]!=-1)
{
temp->path2[j]=path[j];
j = j+1;
}
temp->path2[j]=-1;
printf("  and the cost is %f\n",alternatepathcost);

temp->min_cost = pathcost + alternatepathcost;

/*
if(pathcost<alternatepathcost)
{
temp->min_cost = pathcost;
}
else
{
temp->min_cost = alternatepathcost;
}
*/
//printf("t1= %u,",head2);
//printf("t2= %u\n",temp);
printf("primary path cost = %f,alternate path cost= %f,sum of two costs = %f \n\n",pathcost,alternatepathcost,temp->min_cost); 
}
if(initial_t2!=NULL)
{
twopath **x,*x1,*x2;
//x1 = (twopath *)malloc(sizeof(twopath));
//x2 = (twopath *)malloc(sizeof(twopath));
x= &x1;
x1= head2;
while(1)
{
if((temp->min_cost)<=((*x)->min_cost))
{
if((*x)==head2)
{
x2= (*x);
*x = temp;
head2 = temp;
temp->next = x2;
twopath *tem1;
tem1 = (twopath *)malloc(sizeof(twopath));
tem1 = (*x);
while(tem1->next !=rear2)
{
tem1 =tem1->next;
}
rear2 = tem1->next;
rear2->next = NULL;
break;
}
else
{
x2 = (*x);
(*x)= temp;
temp->next = x2;
twopath *tem2;
tem2 = (twopath *)malloc(sizeof(twopath));
tem2 = (*x);
while(tem2->next!=rear2)
{
tem2 =tem2->next;
}
rear2 = tem2->next;
rear2->next = NULL;
break;
}
}
else
{if((*x)->next == (rear2->next))
	{
	//(*x)->next = temp;
	rear2 = temp;
	break;
	}

	else
	{
	x = &((*x)->next);
	}
}
}//end of while
}//end of stock insert if
printpath(x->next,e_cost1);
}//end of first if
}

void initialize(int source,struct vertex1 vertex[])
{
///printf("\nfrom intialize module %u\n",head2);
vertex[source].value=0;
vertex[source].index = source;
int l,m;
if(source>0)
{
	for(l=0;l<source;l++)
	{
		vertex[l].value=MAX;
		vertex[l].index=l;
	}
}

if(source<(NO_OF_VERTICES-1))
{
	for( m=source+1;m<NO_OF_VERTICES;m++)
	{
		vertex[m].value=MAX;
		vertex[m].index = m;
	}
}

//printf("\n%f\n", vertex[source].value);
}

void heapform(struct vertex1  arr[])
{
struct vertex1 ar[NO_OF_VERTICES];
//printf("entered heapform\n");
//struct vertex1 ar[i];//this can be done in o(n) time sta ting from the last node check to it's parent

//satrt from last node :give numbers as 12345.. to array elements instad of 0 1 2 
//check for it's parent
/*
int k;
for(k=0;k<i;k++)
{
printf("%d,",arr[k].value);
}
printf("\n");

int j;
for(j=i-1;j>=0;j--)
{
if(arr[j].value < arr[((j+1)/2)-1].value)
{
struct vertex1 temp;
temp = arr[j];
arr[j]=arr[((j+1)/2)-1];
arr[(((j+1)/2)-1)] = temp;
}
 
  
}
 
 */
 
  
int j;
for(j=0;j<NO_OF_VERTICES;j++)
{
ar[j].value=arr[j].value;
ar[j].index = arr[j].index;
int real;
real = j+1;
if(real>1)
{
	if((real%2)==1)
		{
		while((real/2)>=1)
		{
			if((ar[(real/2)-1].value)>(ar[real-1].value))
				{
					int k,k1;
					struct vertex1 v1;
					v1= ar[real-1];
					ar[real-1]=ar[(real/2)-1];
					ar[(real/2)-1]=v1;
					real = real/2;
				}
			else
				{
					break;
				}
		}
		}

	if(real%2==0)
	{
		while((real/2)>=1)
		{
			if((ar[(real/2)-1].value)>(ar[real-1].value))
			{
				int l,l1;
				
				struct vertex1 v1;
				v1= ar[real-1];
				ar[real-1]=ar[(real/2)-1];
				ar[(real/2)-1]=v1;
				real = real/2;
				
			}
			else
			{
				break;
			}
		
		}
	}
}
}
int m;
for(m=0;m<NO_OF_VERTICES;m++)
{
struct vertex1 v2;
v2 = arr[m];
arr[m]= ar[m];
}
}

float  dijkstra(int edgenumber2[][NO_OF_VERTICES],int epresent[][NO_OF_VERTICES],struct vertex1 calvertex[],int pat[],float edgecost2[])
{
//printf("entered dijkstra\n");
int len[NO_OF_VERTICES];
int path[NO_OF_VERTICES][NO_OF_VERTICES];
int y1,y2,y3;
	for(y1=0;y1<NO_OF_VERTICES;y1++)
		{		
			len[y1]=0;
		}

	for(y2=0;y2<NO_OF_VERTICES;y2++)
		{
			for(y3=0;y3<NO_OF_VERTICES;y3++)
				{
					path[y2][y3]=-1;
				}
		}
struct vertex1 calvertex3[NO_OF_VERTICES];//used to caluclate the minimum elements its function is to store visited vertices woth value 3000 and pass those value to calvertex1 which will bring the minimum vertex .if the vertex 
struct vertex1 calvertex1[NO_OF_VERTICES];//it just a representative for calvetesx3 to go to the function heapform()
struct vertex1 calvertex2[NO_OF_VERTICES];//stores the constant elements
int j;
	for(j=0;j<NO_OF_VERTICES;j++)
		{
		calvertex1[j]=calvertex[j];
		calvertex2[j]=calvertex[j];
		calvertex3[j]=calvertex[j];
		}
heapform(calvertex1);
int m,ind;
	for(m=0;m<NO_OF_VERTICES;m++)
		{
			ind =calvertex1[0].index;
		//printf("entered dij for loop%d\n",ind);
			int k;
			for(k=0;k<NO_OF_VERTICES;k++)
				{	
					if(k!=ind)
						{

							if(epresent[ind][k]!=0)
								{
								//printf("entered here");
                                    int k1 = edgenumber2[ind][k];//get the edge number
				  //  printf("\n%f\n",edgecost2[k1]);
				    			if(edgecost2[k1]!=0)
							{
                                    			if((1/edgecost2[k1]>0))
				    				{
								//printf("\n\nedgecost is %d  %d,,cost = %f\n",ind,k,edgecost2[k1]);
				    float k2 = (1/ edgecost2[k1]);// assign that edge capacity to k2

									if(((calvertex1[0].value+k2)>0)&&((calvertex1[0].value +k2 )<calvertex2[k].value))
										{
											calvertex2[k].value = calvertex1[0].value+k2;
											//printf("\ncalvertex value %f\n",calvertex2[k].value);
											calvertex3[k].value= calvertex1[0].value+k2;
											int y4=0;//initializing the target vertex
											while(path[k][y4]!=(-1))
												{
													path[k][y4]=-1;
													y4 = y4+1;
												}
											int y5=0;
											while(path[ind][y5]!=(-1))
												{
													path[k][y5]=path[ind][y5];
													y5 = y5+1;
												}
											path[k][y5]=ind;	
											len[k]= y5+1;

										}

								}
								}
								}

						}			 


				}	

int l;

	for(l=0;l<NO_OF_VERTICES;l++)
		{
		calvertex1[l]= calvertex3[l];
		}	

calvertex1[ind].value = 3000;
calvertex3[ind].value = 3000;

//printf("heapfrom was called\n");
heapform(calvertex1);

	}	


//printf("cost is %d,and the path is ",calvertex2[dest].value);
int j1=0;
int a1,a2,a3;
//printf("\nentered path loop %d\n'",path[dest][j1]);
while(path[dest][j1]!=-1)
{
//printf("entered while loop\n");
pat[j1]=path[dest][j1];//storing th verteices in the path from source to destination
printf("%d-",path[dest][j1]);
a1 = path[dest][j1];
a2= path[dest][j1+1];
if(path[dest][j1+1]!=-1)
{
epresent[a1][a2]=0;
epresent[a2][a1]=0;
}
j1 = j1+1;
}
//printf("from outside while loop\n");
if(path[dest][0]!=-1)//to check if algorithm does not working 
{
pat[j1]= dest;
pat[j1+1]=-1;
//printf("from if loop\n");
epresent[a1][dest]=0;
epresent[dest][a1]=0;
printf("%d",dest);
return calvertex2[dest].value;
}
else
{
pat[0]=-1;
printf("\nalternate path dosn't exist\n");
return COST_ZERO;

}
}//end of dijkstra


int w_assign(int pat[],int elength[][NO_OF_VERTICES]/*edge and corrosponding available wavelength*/,int e_versus_wc[]/* wave length capacity*/,float e_cost[])
{
int edge[NO_OF_VERTICES],no_of_edges;
    int j=0,k,l;
      while(pat[j+1]!=-1)
      {
          k= pat[j];l= pat[j+1];//get vertices numbers
          edge[j]=edgenumber[k][l];//get edgenumber corrosponing to that vertices
	  j=j+1;
      }

      edge[j]=-1;//end of edges in edge array
          no_of_edges = j;
	  j=0;
      l=0;
      _Bool index[NO_OF_WAVELENGTHS];
     
/*     
while(edge[j]!=-1)
{
l= edge[j];
if(e_versus_wc[l]==0)
{
return 0;//if number of available wavelengths are zero
}
j= j+1;
}
*/
      for(l=0;l<NO_OF_WAVELENGTHS;l++)
      {
          int j=0,h,k,q;
	  _Bool sa;
          index[l]=1;
          while(edge[j]!=-1)
          {
                   h = edge[j];//get corresponding egde number
		   q = elength[h][l];
		   if(q>0)
		   {
		   sa =1;
		   }
		   else 
		   {
		   sa =0;
		   }
                 index[l] = ((index[l]) &&(sa==1));//elength >1 also
             j = j+1;
           }
      } 
               
      j=0;
      _Bool test=0;//check the assignment of a wave length
      
      for(j=0;j<NO_OF_WAVELENGTHS;j++)
      {        
	test = (test || (index[j]));          
                     
      } 
if(test==1)
{
    int j1;
    for(j1=0;j1<NO_OF_WAVELENGTHS;j1++)
    {
     if(index[j1]==1)
     {
         int k1=0,j2;
         for(k1=0;k1<no_of_edges;k1++)
         {
             
           j2 = edge[k1];//to get the edge number
             elength[j2][j1]= elength[j2][j1]- 1;// decreasing the amount of different wavelengths available to a particular edged
//	   e_versus_wc[j2] = e_versus_wc[j2] -1;//decreasing the corresponding edge's capacity in terms of total different wavelengths avaialble 
         e_cost[j2]= e_cost[j2]-1;//incrementing the edge cost of that particular edge                 
         }         
         wavelengthassigned = j1;
       break;                                       
     }      
    }

printf("\n****From assignment ***\n");
int c3,c4;
for(c3=0;c3<10;c3++)
 {
  for(c4=0;c4<6;c4++)
  {

printf("%d,",elength[c3][c4]);	
  }
  printf("\n");
}  

    return (j1+1);//assignment is possible  
}
else
{
    return 0;//no assignment possible
}        


}  


